#!/bin/bash
set -e

if [ ! -e '/var/www/html/version.php' ]; then
    tar cf - --one-file-system -C /usr/src/owncloud . | tar xf -
    # Set permissions, from https://doc.owncloud.org/server/8.2/admin_manual/installation/installation_wizard.html#strong-perms-label
    if [ ! -d /var/www/html/data ]; then
	mkdir -p /var/www/html/data
    fi
    if [ ! -d /var/www/html/assets ]; then
	mkdir -p /var/www/html/assets
    fi
    
    find /var/www/html/ -type f -print0 | xargs -0 chmod 0640
    find /var/www/html/ -type d -print0 | xargs -0 chmod 0750
    
    chown -R root:www-data /var/www/html/
    chown -R www-data:www-data /var/www/html/apps/
    chown -R www-data:www-data /var/www/html/config/
    chown -R www-data:www-data /var/www/html/data/
    chown -R www-data:www-data /var/www/html/themes/
    chown -R www-data:www-data /var/www/html/assets/
    chown www-data:www-data /var/www/html/.user.ini

    chmod +x /var/www/html/occ
    
    # Check if there is a custom CA Certificate to add    
    if [ -e '/tmp/mycacert.crt' ]; then
	echo "We have a custom CA Certificate to add to our bundle"
	cat '/tmp/mycacert.crt' > /usr/local/share/ca-certificates/owncloud-cacert.crt
	update-ca-certificates
    fi
fi

# Configure redis-server (from /etc/redis/redis.conf)
if [ ! -e '/etc/redis/redis-custom.conf' ]; then
    mv /redis-conf /etc/redis/redis-custom.conf
fi

# Configure supervisord
if [ ! -e '/etc/supervisor/conf.d/owncloud.conf' ]; then
    cat <<EOF > /etc/supervisor/conf.d/owncloud.conf
[supervisord]
nodaemon=true

[program:php-fpm]
command=/usr/local/sbin/php-fpm
autorestart=true

[program:redis]
command=/usr/bin/redis-server /etc/redis/redis-custom.conf
autorestart=true
EOF

fi

if [ "x$1" = "xoc_start" ]; then
    exec /usr/bin/supervisord > /dev/null 2>&1
fi

exec "$@"
