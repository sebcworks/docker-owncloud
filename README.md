ownCloud 8 Container
====================

Description
-----------

A custom ownCloud container with LDAP lib installed, redis-server and supervisor to manage processes.

It is based on the [official ownCloud:fpm container](https://hub.docker.com/_/owncloud/).

You may use it with the volumes created by the data container sebcworks/owncloud-data (https://hub.docker.com/r/sebcworks/owncloud-data/) :

* /var/www/html/data/
* /var/www/html/config/

It exports directory `/var/www/html` as a VOLUME.

I also put an example of a docker-compose.yml file if you wish.

Usage
-----

*If easily use it with my the docker-compose.yml file*

    git clone https://sebcworks@bitbucket.org/sebcworks/docker-owncloud.git

    cd docker-owncloud

    cp example-docker-compose.yml docker-compose.yml

    docker-compose up -d
    
Don't forget to change the configuration of ownCloud if you want to use the redis-server as memcache:
    
    'memcache.locking' => '\OC\Memcache\Redis',
    'memcache.local' => '\OC\Memcache\Redis',
    'redis' => array(
        'host' => '/tmp/redis/redis.sock',
	'port' => 0,
	'timeout' => 0.0,
     ), 
    
